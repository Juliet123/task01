﻿using System;
using System.Collections.Generic;
using System.Text;
using Task01.Models;

namespace Task01.Services
{
    public class PersonServices
    {
        private static Person person;
        public PersonServices(string fname, string lname, int age)
        {
            person = new Person()
            {
                FirstName = fname,
                LastName = lname,
                Age = age
            };
        }

        public static void GetPersonInfo()
        {
            Console.WriteLine($"My name is {person.LastName} {person.FirstName} and I am {person.Age} years old!");
        }
    }
}
