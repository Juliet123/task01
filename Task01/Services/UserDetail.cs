﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task01.Services
{
    class UserDetail
    {
        public static string getName()
        {
            //get user's name
            Console.WriteLine("Enter Your Name");
           
            string username = Console.ReadLine();
            return username;
        }
        public static int getAge()
        {
            
            int age = 0;

            //get user's age
            Console.WriteLine("Enter your Date of Birth");
            
            string dateofbirth = Console.ReadLine();


            try
            {
                DateTime dob = Convert.ToDateTime(dateofbirth);
                DateTime currentDate = DateTime.Today;

                age = currentDate.Year - dob.Year;
                if (dob > currentDate.AddYears(-age))
                    age--;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            return age;
        }

    }
}
