﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task01.Models
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }
    }
}
